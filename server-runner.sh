#!/bin/bash
DEST=${HOME}/.local/bin
BINARY_DEST=${DEST}/server-runner
INSTALLATION_DIR=${HOME}/.server-runner/

# Create symlink if it doesn't exist
if [[ ! -f ${BINARY_DEST} ]]
then
    mkdir -p ${DEST}
    ln -s ${HOME}/.server-runner/runner.py ${BINARY_DEST}
    chmod +x ${BINARY_DEST}
fi

git -C ${INSTALLATION_DIR} pull --rebase --stat origin master &> /dev/null
