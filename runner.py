#!/usr/bin/python3
import os
import stat
import argparse
import subprocess
import logging
from pathlib import Path
from time import sleep

logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(message)s', datefmt='%H:%M:%S')

servers_path = os.environ.get('SERVERS_PATH')
startup_name = os.environ.get('STARTUP_NAME') or 'start.sh'

server_list = []

def isExists(screenName):
    return True if subprocess.call(f'screen -list | grep -q {screenName}', shell=True) == 0 else False

def start(server, server_path):
    logging.info(f'[{server}] Starting...')
    if isExists(server):
        logging.info(f'[{server}] Server is already started.')
        return

    script = (server_path / startup_name)

    if not script.exists():
        logging.info(f'[{server}] Startup script not found.')
        return
    
    os.chmod(script, os.stat(script).st_mode | stat.S_IEXEC)
    subprocess.call(f'screen -dmS {server} ./start.sh', shell=True, cwd=server_path)

def stop(server, server_path):
    i = 0
    if isExists(server):
        logging.info(f'[{server}] Sending stop signal.')
        while isExists(server):
            if i <= 2:
                subprocess.call(f'screen -S {server} -X stuff "stop^M"', shell=True)
                subprocess.call(f'screen -S {server} -X stuff "end^M"', shell=True)
            sleep(1)
        return
    logging.info(f'[{server}] Server already stopped...')
    

def restart(server, server_path):
    stop(server, server_path)
    start(server, server_path)

def cli_handler(args, handler):
    server = args.server

    server_name_list = list(map(lambda x: x['name'], server_list))
    finded_server = next(filter(lambda x: x['name'] == server, server_list), False)

    if server != 'all' and finded_server == False:
        logging.info(f'[{server}] Server not found...')
        return

    if server == 'all':
        for server in server_list:
            handler(server['name'], server['path'])
        return
        
    handler(finded_server['name'], finded_server['path'])

def cli_start(args):
    cli_handler(args, start)

def cli_stop(args):
    cli_handler(args, stop)

def cli_restart(args):
    cli_handler(args, restart)


parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(title='Supported arguments')

start_parser = subparsers.add_parser('start', help='Start server')
start_parser.add_argument('server', help='Server name ( Supported: all )')
start_parser.set_defaults(func=cli_start)

stop_parser = subparsers.add_parser('stop', help='Stop server')
stop_parser.add_argument('server', help='Server name ( Supported: all )')
stop_parser.set_defaults(func=cli_stop)

restart_parser = subparsers.add_parser('restart', help='Restart server')
restart_parser.add_argument('server', help='Server name ( Supported: all )')
restart_parser.set_defaults(func=cli_restart)


if __name__ == '__main__':
    if not servers_path:
        logging.error('You must specify SERVERS_PATH env')
        exit(1)
    with open(Path(servers_path) / 'servers.txt') as file:
        # server_list = [str.strip(server) for server in file]
        server_paths = [str.strip(server) for server in file]
        for path in server_paths:
            if not path:
                continue

            name = Path(path).name

            server_list.append({
                'name': str(name),
                'path': Path(servers_path) / path
            })
    args = parser.parse_args()
    if not vars(args):
        parser.print_usage()
    else:
        args.func(args)